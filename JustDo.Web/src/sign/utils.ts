const MIN_PASSWORD_LENGTH = 8;
export const PASSWORD_EMPTY_MSG = `The password must contain not less than ${MIN_PASSWORD_LENGTH} symbols`;
export const EMAIL_EMPTY_MSG = 'The address must not be empty';

export interface IsValidResult {
  isValid: boolean;
  errorMessage?: string;
}

function getErrorResult(errorMessage: string): IsValidResult  {
  return {
    isValid: false,
    errorMessage
  };
}

export function isValidEmail (email: string) {
  if (email === undefined || email === null || email.length === 0) {
    return getErrorResult(EMAIL_EMPTY_MSG);
  }

  if (!email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
    return getErrorResult('The address is not valid');
  }

  return { isValid: true };
}

export function isValidPassword (pass: string): IsValidResult {
  if (pass === undefined || pass === null || pass.length < MIN_PASSWORD_LENGTH) {
    return getErrorResult(PASSWORD_EMPTY_MSG);
  }

  if (pass.match(/\s/)) {
    return getErrorResult('The password must not contain white spaces');
  }

  if (!pass.match(/[A-Z].*[A-Z]/)) {
    return getErrorResult('The password must contain not less than 2 capital letters');
  }

  if (!pass.match(/\W/)) {
    return getErrorResult('The password must contain not less than 1 special symbol');
  }

  if (!pass.match(/\d/)) {
    return getErrorResult('The password must contain not less than 1 digit');
  }

  return { isValid: true };
}

export function isEqualPasswords(pass: string, pass2: string) {
  if (pass !== pass2) {
    return getErrorResult('Password do not match');
  }

  return { isValid: true };
}