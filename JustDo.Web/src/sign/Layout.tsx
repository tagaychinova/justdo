import * as React from 'react';
import { ReactComponent as LogoSvg } from '../images/logo.svg';

import * as css from './Layout.module.scss';

interface LayoutProps {
  children: any;
}

export class Layout extends React.PureComponent<LayoutProps> {
  render() {
    const { children } = this.props;

    return (
      <div className={css.container}>
        <div className={css.logoBackgroundContainer}>
          <div className={css.logoBackgroundImg}>
            <div className={css.logoImg}>
              <LogoSvg />
            </div>
          </div>
        </div>
        <div className={css.mainContent}>
          {children}
        </div>
      </div>
    );
  }
}