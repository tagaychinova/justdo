import {environment} from '@src/environment';

interface CreateAccountResponse {
  isSuccess: boolean;
  errorMessage: string;
}

export function createAccount(login: string, password: string): Promise<CreateAccountResponse> {
  return fetch(`${environment.apiUrl}account/create`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      login,
      password
    })
  }).then(response => response.json())
}

interface LoginResponse {
  isSuccess: boolean;
  accessToken: string;
  errorMessage: string;
}

export function login(login: string, password: string): Promise<LoginResponse> {
  return fetch(`${environment.apiUrl}token`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      login,
      password
    })
  }).then(response => response.json())
}