import * as React from 'react';
import classNames from 'classnames';
import { RoutePaths } from '@src/Routes';
import { JdInput, JdButton, JdButtonType } from '@src/common';
import ArrowLeftSvg from '@src/images/ic_arrow_left.svg';

import * as css from './Layout.module.scss';
import * as mainCss from '@src/index.module.scss';

export class ForgotPassword extends React.Component<any, any> {
  render() {
    return (
      <>
        <a className={classNames(css.forgotPassword, css.goBack)} href={RoutePaths.SignIn}>
          <img src={ArrowLeftSvg} />
        </a>
        <div className={css.signContainer}>
          <div className={css.formContainer}>
            <div className={mainCss.form}>
              <div className={mainCss.formRow}>
                <span className={mainCss.formTitle}>Forgot Password</span>
              </div>
              <div className={classNames(mainCss.formRow, mainCss.centerText)}>
                <span className={mainCss.text}>Please enter your email below to receive your<br/> password reset instruction</span>
              </div>
              <div className={mainCss.formRow}>
                <JdInput placeholder='E-mail' />
              </div>
              <div className={mainCss.formRow}>
                <JdButton text='Send' type={JdButtonType.Primary} style={({ width: '257px'})} />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}