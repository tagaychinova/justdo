import * as React from 'react';
import { createAccount } from '@src/sign/api';
import { RoutePaths } from '@src/Routes';
import { Footer } from './Footer';
import { JdInput, JdInputType, JdButton, JdButtonType } from '@src/common';
import { EMAIL_EMPTY_MSG, PASSWORD_EMPTY_MSG, isValidEmail, isValidPassword } from '@src/sign/utils';
import { AppContext, IAppContextValue } from '@src/AppContext';

import * as css from './Layout.module.scss';
import * as mainCss from '@src/index.module.scss';

interface InputState {
  value: string;
  error: string;
  showError: boolean;
}

const emptyInputState = (error: string) => ({
  value: '',
  error,
  showError: false
});

interface SignUpState {
  email: InputState;
  password: InputState,
  password2: InputState;
  serverError?: string;
}

export class SignUp extends React.Component<{}, SignUpState> {
  static contextTypes = {
    router: true
  };

  state: SignUpState = {
    email: emptyInputState(EMAIL_EMPTY_MSG),
    password: emptyInputState(PASSWORD_EMPTY_MSG),
    password2: emptyInputState(PASSWORD_EMPTY_MSG)
  };

  componentDidUpdate() {
    const { password, password2 } = this.state;

    if (password.showError &&
      password2.showError &&
      password.error.length === 0 &&
      password2.error.length === 0 &&
      password.value !== password2.value
    ) {
      this.setState({
        password2: {
          ...password2,
          error: 'Password do not match'
        }
      });
    }
  }

  handleEmailInput = (e: React.ChangeEvent<any>) => {
    const email = e.target.value;
    const isValidResult = isValidEmail(email);

    this.setState({
      email: {
        ...this.state.email,
        value: email,
        error: isValidResult.isValid ? '' : isValidResult.errorMessage || ''
      }
    });
  };

  handlePasswordInput = (e: React.ChangeEvent<any>) => {
    const password = e.target.value;
    const isValidResult = isValidPassword(password);

    this.setState({
      password: {
        ...this.state.password,
        value: password,
        error: isValidResult.isValid ? '' : isValidResult.errorMessage || ''
      }
    });
  };

  handlePassword2Input = (e: React.ChangeEvent<any>) => {
    const password2 = e.target.value;
    const isValidResult = isValidPassword(password2);

    this.setState({
      password2: {
        ...this.state.password2,
        value: password2,
        error: isValidResult.isValid ? '' : isValidResult.errorMessage || ''
      }
    });
  };

  signUpOnClick = (appContext: IAppContextValue) => () => {
    const newState = {...this.state};

    const inputs = [newState.email, newState.password, newState.password2];
    inputs.map(i => { i.showError = true; });

    if (inputs.find(i => i.error.length > 0)) {
      this.setState(newState);
      return;
    }

    appContext.startLoading();
    createAccount(newState.email.value, newState.password.value)
      .then(data => {
        appContext.endLoading();

        if (data.isSuccess) {
          this.context.router.history.push(RoutePaths.SignIn)
        } else {
          newState.serverError = data.errorMessage;
          this.setState(newState);
        }
      });
  };

  enableShowError = (getInputState: (entity: SignUpState) => InputState) => {
    return () => {
      const newState = {...this.state};
      const inputState = getInputState(newState);
      inputState.showError = true;
      this.setState(newState);
    };
  };

  render() {
    const { email, password, password2, serverError } = this.state;

    return (
      <AppContext.Consumer>
        {
          (appContext: IAppContextValue) =>
            (
              <div className={css.signContainer}>
                <div className={css.formContainer}>
                  <div className={mainCss.form}>
                    <div className={mainCss.formRow}>
                      <span className={mainCss.formTitle}>Sign Up</span>
                    </div>
                    <div className={mainCss.formRow}>
                      <JdInput
                        placeholder='E-mail'
                        value={email.value}
                        showError={email.showError}
                        errorMessage={email.error}
                        onChange={this.handleEmailInput}
                        onBlur={this.enableShowError(s => s.email)} />
                    </div>
                    <div className={mainCss.formRow}>
                      <JdInput
                        placeholder='Password'
                        type={JdInputType.Password}
                        value={password.value}
                        showError={password.showError}
                        errorMessage={password.error}
                        onChange={this.handlePasswordInput}
                        onBlur={this.enableShowError(s => s.password)}/>
                    </div>
                    <div className={mainCss.formRow}>
                      <JdInput
                        placeholder='Confirm password'
                        type={JdInputType.Password}
                        value={password2.value}
                        showError={password2.showError}
                        errorMessage={password2.error}
                        onChange={this.handlePassword2Input}
                        onBlur={this.enableShowError(s => s.password2)} />
                    </div>
                    <div className={mainCss.formRow}>
                      <JdButton
                        text='Sign Up'
                        type={JdButtonType.Primary}
                        style={({ width: '257px'})}
                        onClick={this.signUpOnClick(appContext)} />
                    </div>
                    <div className={mainCss.formRow}>
                      <span className={mainCss.text}>I already have an account.</span>
                      &nbsp;
                      <a className={mainCss.primaryLink} href={RoutePaths.SignIn}>Sign In</a>
                    </div>
                    <div className={css.serverError}>{serverError}</div>
                  </div>
                </div>
                <Footer />
              </div>
            )
        }
      </AppContext.Consumer>
    );
  }
}