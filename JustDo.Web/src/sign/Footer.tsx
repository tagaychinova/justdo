import * as React from 'react';
import classNames from 'classnames';
import { RoutePaths } from '@src/Routes';

import * as css from './Layout.module.scss';
import * as mainCss from '@src/index.module.scss';

export class Footer extends React.Component<{}> {
  render() {
    return (
      <div className={classNames(mainCss.text, css.footer)}>
        By accessing your account, you agree to our
        <br/>
        <a className={mainCss.primaryLink} href={RoutePaths.TermsConditions}>Terms conditions</a> and <a className={mainCss.primaryLink} href={RoutePaths.PrivacyPolicy}>Privacy Policy</a>
      </div>
    );
  }
}