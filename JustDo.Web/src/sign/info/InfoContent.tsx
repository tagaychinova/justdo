import * as React from 'react';
import classNames from 'classnames';
import ArrowLeftSvg from '@src/images/ic_arrow_left.svg';

import * as css from './InfoContent.module.scss';
import * as mainCss from '@src/index.module.scss';

interface InfoContentProps {
  title: string;
  children: any;
}

export class InfoContent extends React.Component<InfoContentProps> {
  static contextTypes = {
    router: () => true
  };

  render() {
    const { title, children } = this.props;

    return (
      <div className={css.infoContent}>
        <div className={css.titleContainer}>
          <a className={css.goBack} onClick={this.context.router.history.goBack}>
            <img src={ArrowLeftSvg} />
          </a>
          <div className={classNames(mainCss.mainTitle, css.mainTitle)}>{title}</div>
        </div>
        <div>
          {children}
        </div>
      </div>
    );
  }
}