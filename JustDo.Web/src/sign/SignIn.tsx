import * as React from 'react';
import { login } from '@src/sign/api';
import { RoutePaths } from '@src/Routes';
import { Footer } from './Footer';
import { JdInput, JdInputType, JdButton, JdButtonType } from '@src/common';
import { AppContext, IAppContextValue } from '@src/AppContext';

import * as css from './Layout.module.scss';
import * as mainCss from '@src/index.module.scss';

interface SignInState {
  email: string;
  password: string;
  serverError: string;
}

export class SignIn extends React.Component<{}, SignInState> {
  state: SignInState = {
    email: '',
    password: '',
    serverError: ''
  };

  signInOnClick = (appContext: IAppContextValue) => () => {
    const { email, password } = this.state;

    if (email.length === 0 || password.length === 0) {
      return;
    }

    appContext.startLoading();
    login(email, password)
      .then(data => {
        appContext.endLoading();
        if (data.isSuccess) {
          localStorage.setItem('jwt', data.accessToken);
          alert(`Set JWT: ${data.accessToken}`);
        } else {
          this.setState({
            serverError: data.errorMessage
          });
        }
      });
  };

  handleEmailInput = (e: React.ChangeEvent<any>) => {
    const email = e.target.value;

    this.setState({
      email
    });
  };

  handlePasswordInput = (e: React.ChangeEvent<any>) => {
    const password = e.target.value;

    this.setState({
      password
    });
  };

  render() {
    const { email, password, serverError } = this.state;

    return (
        <AppContext.Consumer>
        {
          (appContext: IAppContextValue) =>
            (
              <div className={css.signContainer}>
                <div className={css.formContainer}>
                  <div className={mainCss.form}>
                    <div className={mainCss.formRow}>
                      <span className={mainCss.formTitle}>Sign In</span>
                    </div>
                    <div className={mainCss.formRow}>
                      <JdInput
                        placeholder='E-mail'
                        value={email}
                        onChange={this.handleEmailInput} />
                    </div>
                    <div className={mainCss.formRow}>
                      <JdInput
                        placeholder='Password'
                        type={JdInputType.Password}
                        value={password}
                        onChange={this.handlePasswordInput} />
                    </div>
                    <div className={mainCss.formRow}>
                      <a className={mainCss.primaryLink} href={RoutePaths.ForgotPassword}>Forgot password?</a>
                    </div>
                    <div className={mainCss.formRow}>
                      <JdButton
                        text='Sign In'
                        type={JdButtonType.Primary}
                        style={({ width: '257px'})}
                        onClick={this.signInOnClick(appContext)} />
                    </div>
                    <div className={mainCss.formRow}>
                      <a className={mainCss.primaryLink} href={RoutePaths.SignUp}>Sign Up</a>
                    </div>
                    <div className={css.serverError}>{serverError}</div>
                  </div>
                </div>
                <Footer />
              </div>
            )
        }
        </AppContext.Consumer>
    );
  }
}