import * as React from 'react';
import { Route, Switch } from 'react-router';
import { SignIn } from './sign/SignIn';
import { SignUp } from './sign/SignUp';
import { ForgotPassword } from './sign/ForgotPassword';
import { Layout as SignLayout } from './sign/Layout';
import { TermsConditions} from './sign/info/TermsConditions';
import { PrivacyPolicy } from './sign/info/PrivacyPolicy';

export class RoutePaths {
  static SignIn = '/';
  static SignUp = '/sign-up';
  static ForgotPassword = '/forgot-password';
  static TermsConditions = '/terms-conditions';
  static PrivacyPolicy = '/privacy-policy';
}

export default class Routes extends React.Component<any, any> {
  render() {
    return (
      <Switch>
        <SignLayout>
          <Route exact path={RoutePaths.SignIn} component={SignIn} />
          <Route exact path={RoutePaths.SignUp} component={SignUp as any} />
          <Route exact path={RoutePaths.ForgotPassword} component={ForgotPassword} />
          <Route exact path={RoutePaths.TermsConditions} component={TermsConditions} />
          <Route exact path={RoutePaths.PrivacyPolicy} component={PrivacyPolicy} />
        </SignLayout>
      </Switch>
    );
  }
}