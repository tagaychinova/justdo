import * as React from 'react';
import classNames from 'classnames';
import * as css from './JdButton.module.scss';

export enum JdButtonType {
  Primary,
  Secondary
}

interface JdButtonProps {
  text: string;
  type?: JdButtonType;
  style?: { [id: string]: string; };
  onClick?: () => void;
}

export class JdButton extends React.Component<JdButtonProps> {
  static defaultProps: JdButtonProps = {
    type: JdButtonType.Secondary,
    text: '',
  };

  render() {
    const { text, type, style, onClick } = this.props;

    return (
      <button
        className={classNames(css.jdBtn, (type === JdButtonType.Primary) && css.jdPrimaryBtn)}
        style={style}
        onClick={onClick}
      >{text}</button>
    );
  }
}