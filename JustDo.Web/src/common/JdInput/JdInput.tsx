import * as React from 'react';
import classNames from 'classnames';
import EyeUnvisibleSvg from './img/ic_eye_unvisible.svg';
import EyeVisibleSvg from './img/ic_eye_visible.svg';
import * as css from './JdInput.module.scss';

export enum JdInputType {
  Text = 'text',
  Password = 'password'
}

interface JdInputProps {
  type?: JdInputType;
  placeholder?: string;
  value?: string;
  errorMessage?: string;
  showError?: boolean,
  onChange?: React.ChangeEventHandler;
  onBlur?: React.ChangeEventHandler;
}

interface JdInputState {
  isEyeVisible?: boolean;
}

export class JdInput extends React.Component<JdInputProps, JdInputState> {
  static defaultProps: JdInputProps = {
    type: JdInputType.Text,
    showError: true
  };

  state = {
    isEyeVisible: true
  };

  togglePasswordVisible = () => {
    const { isEyeVisible } = this.state;

    this.setState({
      isEyeVisible: !isEyeVisible
    });
  };

  render() {
    let { type, value, showError, errorMessage, onChange, onBlur, placeholder } = this.props;
    const { isEyeVisible } = this.state;

    const isPassword = type === JdInputType.Password;
    if (isPassword && !isEyeVisible) {
      type = JdInputType.Text;
    }

    return (
      <div className={css.jdInputWrapper}>
        <div className={css.jdInputContainer}>
          <input
            className={css.jdInput}
            type={type}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            placeholder={placeholder} />
          { isPassword && (
            <img
              className={classNames(css.eyeSvg, isEyeVisible &&  css.eyeVisible)}
              src={ isEyeVisible ? EyeVisibleSvg : EyeUnvisibleSvg }
              onClick={this.togglePasswordVisible} />
          ) }
        </div>
        <div className={css.errorMessage}>{showError && errorMessage}</div>
      </div>
    );
  }
}