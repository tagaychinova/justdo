import * as React from 'react';
import SpinnerSvg from './img/spinner.svg';

import * as css from './JdLoading.module.scss';

interface JdLoadingState {
  visible: boolean;
}

export class JdLoading extends React.PureComponent<{}, JdLoadingState> {
  state: JdLoadingState = {
    visible: false
  };

  setVisible(visible: boolean) {
    this.setState({
      visible
    });
  }

  render() {
    const { visible } = this.state;

    if (!visible) {
      return null;
    }

    return (
      <div className={css.jdLoading}>
        <img src={SpinnerSvg} />
      </div>
    );
  };
}