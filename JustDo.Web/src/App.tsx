import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { AppContext, IAppContextValue } from './AppContext';
import Routes from './Routes';
import { JdLoading } from './common/JdLoading';

class App extends React.Component {
  private loadingCount = 0;
  private loadingRef: JdLoading | null = null;

  startLoading = () => {
    const prevIsLoading = this.isLoading();
    this.loadingCount++;
    this.updateLoading(prevIsLoading);
  };

  endLoading = () => {
    const prevIsLoading = this.isLoading();
    this.loadingCount--;
    this.updateLoading(prevIsLoading);
  };

  updateLoading = (prevIsLoading: boolean) => {
    const isLoading = this.isLoading();
    if (prevIsLoading !== isLoading && this.loadingRef) {
      this.loadingRef.setVisible(isLoading);
    }
  };

  isLoading = () => this.loadingCount > 0;

  getAppContext(): IAppContextValue {
    return {
      startLoading: this.startLoading,
      endLoading: this.endLoading
    };
  }

  render() {
    return (
      <AppContext.Provider value={this.getAppContext()}>
        <Router>
          <Routes />
        </Router>
        <JdLoading
          ref={ref => this.loadingRef = ref }
        />
      </AppContext.Provider>
    );
  }
}

export default App;
