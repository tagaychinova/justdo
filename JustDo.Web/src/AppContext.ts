import * as React from 'react';

export interface IAppContextValue {
  startLoading: () => void;
  endLoading: () => void;
}

const defaultValue: IAppContextValue = {
  startLoading: () => {},
  endLoading: () => {}
};

export const AppContext = React.createContext(defaultValue);