CREATE TABLE AppUsers (      
  UserID int IDENTITY(1,1) NOT NULL PRIMARY KEY,      
  Login varchar(255) NOT NULL,
  Password varchar(255) NOT NULL,
  CONSTRAINT U_Login UNIQUE(Login)  
)      
GO   