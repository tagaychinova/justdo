﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using JustDo.Api.Models;

namespace JustDo.Api.Controllers
{
    public class AccountController : Controller
    {
        [HttpPost("/account/create")]
        public CreateUserResponse CreateUser([FromBody] User user)
        {
            using (var db = new JustDoContext())
            {
                var isUserExist = db.AppUsers.Any(u => u.Login == user.Login);
                if (isUserExist)
                {
                    return new CreateUserResponse
                    {
                        IsSuccess = false,
                        ErrorMessage = "User with this login already exists"
                    };
                }

                try
                {
                    db.AppUsers.Add(new AppUsers
                    {
                        Login = user.Login,
                        Password = EncodePassword(user.Password)
                    });

                    db.SaveChanges();
                    return new CreateUserResponse
                    {
                        IsSuccess = true
                    };
                }
                catch (Exception ex)
                {
                    return new CreateUserResponse
                    {
                        IsSuccess = false,
                        ErrorMessage = ex.Message
                    };
                }
            }
        }

        [HttpPost("/token")]
        public TokenResponse Token([FromBody] User user)
        {
            var identity = GetIdentity(user.Login, user.Password);
            if (identity == null)
            {
                return new TokenResponse {
                    IsSuccess = false,
                    ErrorMessage = "Invalid username or password"
                };
            }

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return new TokenResponse
            {
                IsSuccess = true,
                AccessToken = encodedJwt
            };
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            var encodedPassword = EncodePassword(password);
            using (var db = new JustDoContext())
            {
                var person = db.AppUsers.FirstOrDefault(x => x.Login == username && x.Password == encodedPassword);
                if (person != null)
                {
                    var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login)
                };
                    ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                    return claimsIdentity;
                }
            }

            return null;
        }

        private static string EncodePassword(string password) {
            var data = System.Text.Encoding.ASCII.GetBytes(password);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            var hash = System.Text.Encoding.ASCII.GetString(data);
            return hash;
        }
    }
}