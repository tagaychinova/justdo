﻿namespace JustDo.Api.Models
{
    public class TokenResponse
    {
        public bool IsSuccess { get; set; }

        public string AccessToken { get; set; }

        public string ErrorMessage { get; set; }
    }
}
