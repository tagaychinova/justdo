﻿namespace JustDo.Api.Models
{
    public class CreateUserResponse
    {
        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }
    }
}
