﻿using System;
using System.Collections.Generic;

namespace JustDo.Api.Models
{
    public partial class AppUsers
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
