﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace JustDo.Api
{
    public class AuthOptions
    {
        public const string ISSUER = "JustDoServer";

        public const string AUDIENCE = "just-do";

        const string KEY = "F35kTb%3W2vrRs96D";

        public const int LIFETIME = 1;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
